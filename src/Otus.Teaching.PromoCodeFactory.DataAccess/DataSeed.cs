﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public static class DataSeed
{
    public static void Seed(ModelBuilder builder)
    {
        builder.Entity<Role>().HasData(FakeDataFactory.Roles);
        builder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        builder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
        builder.Entity<Employee>().HasData(FakeDataFactory.Employees);
        builder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
    }
}